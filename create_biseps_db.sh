#!/bin/bash


if [ "$#" -ne 2 ];
then
    echo "Usage:"
    echo "\$1 : database name"
    echo "\$2 : location of BISEPS \".dat\" files (can be relative path)"
    exit 1
fi


# setup variables
# (strip any trailing "/" from DIR_DATA)
DB_NAME="$1"
DIR_DATA=${2%/}
DIR_SCRIPTS="$PWD"
INDEX_COLUMN="num"


# data files to import
star_kep="WEBwide_kep.dat.1"
star_pop="WEBwide_pop.dat.1"
star_mag="WEBwide_mag.dat.1"

binary_kep="WEB_kep.dat.1"
binary_pop="WEB_pop.dat.1"
binary_mag="WEB_mag.dat.1"




# is data folder valid?
if [ ! -d "$DIR_DATA" ];
then
    echo fatal: invalid directory
    echo "$DIR_DATA" 
    exit 1
fi



# import all single stars 
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$star_kep"    webwide_kep  kep.cols  "$DB_NAME"  $INDEX_COLUMN
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$star_pop"    webwide_pop  pop.cols  "$DB_NAME"  $INDEX_COLUMN
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$star_mag"    webwide_mag  mag.cols  "$DB_NAME"  $INDEX_COLUMN

# import all binaries
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$binary_kep"  web_kep      kep.cols  "$DB_NAME"  $INDEX_COLUMN
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$binary_pop"  web_pop      pop.cols  "$DB_NAME"  $INDEX_COLUMN
"$DIR_SCRIPTS"/process_file.sh   "$DIR_DATA"/"$binary_mag"  web_mag      mag.cols  "$DB_NAME"  $INDEX_COLUMN


