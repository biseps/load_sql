#!/bin/bash --


# create an INDEX on an Sqlite table


# Validate arguments
if [ "$#" -ne 3 ];
then
    echo "Usage:"
    echo "\$1 : database name"
    echo "\$2 : table name"
    echo "\$3 : column to create index on"
    exit 1
fi



# parse command-line arguments
db_name="$1"
table="$2"
column="$3"
index_name="idx_${table}_${column}"


# does database exist?
if [ ! -f "$db_name" ]; then
    echo "fatal: database does not exist:"
    echo "$db_name"
    exit 1
fi


# create column index,
# passing SQL statements as a 'heredoc'

sqlite3 -batch ${db_name} <<EOF
.print
.print 'creating index...'
.print 'may take several minutes...'

.echo on
begin transaction;
create index if not exists ${index_name} on ${table} (${column});
commit;
EOF
