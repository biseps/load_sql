--  E Farrell 2014


.print
.print 'input File: WEBwide_pop.dat.1'
.print 'creating table ...'
begin transaction;

create table webwide_pop 
(
    num     real, 
    mt1     real, 
    mt2     real, 
    Porb    real,
    birth   real,
    death   real,
    evol    text
);

.print 'importing data...'
.separator " "
.import WEBwide_pop.dat.1 webwide_pop

commit;
.print 'data imported'


.print 'creating index...'
begin transaction;
create index idx_popwide_num on webwide_pop (num);
commit;
.print 'index created'


.print 'Number of records imported:'
select count(*) from webwide_pop;


