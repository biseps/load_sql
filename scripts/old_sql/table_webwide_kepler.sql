--  E Farrell 2014


.print
.print 'input File: WEBwide_kepler.dat.1'
.print 'creating table ...'
begin transaction;

create table webwide_kepler 
(
    num         real, 
    mt1         real, 
    reff1       real, 
    teff1       real, 
    lum1        real, 
    mt2         real, 
    reff2       real, 
    teff2       real, 
    lum2        real, 
    Porb        real, 
    birth       real, 
    death       real, 
    massTran    int, 
    kw1         int, 
    kw2         int, 
    bkw         int, 
    met         real, 
    evol        text 
);

.print 'importing data...'
.separator " "
.import WEBwide_kepler.dat.1 webwide_kepler

commit;
.print 'data imported'


.print 'creating index...'
begin transaction;
create index idx_keplerwide_num on webwide_kepler (num);
commit;
.print 'index created'


.print 'Number of records imported:'
select count(*) from webwide_kepler;


