
select  distinct m.num, m.mag1, m.mag2, p.evol  
from    web_mag as m, 
        web_pop as p 
where   (m.num = p.num) and (m.num = 1.0);

select  m.num, m.mag1, m.mag2, p.evol  
from   web_mag as m  LEFT JOIN web_pop as p 
ON     m.num = p.num AND m.num = 1.0;

select num from web_mag where num = 1.0;
select num from web_pop where num = 1.0;

select count(*) from web_mag where num = 1.0;
select count(*) from web_pop where num = 1.0;

create index idx_mag_num on web_mag (num);
