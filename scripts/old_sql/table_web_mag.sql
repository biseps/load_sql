--  E Farrell 2014


.print
.print 'input File: WEB_mag.dat.1'
.print 'creating table ...'
begin transaction;

create table web_mag 
(
    num     real, 
    mag1    real, 
    mag2    real, 
    magBin  real
);

.print 'importing data...'
.print '(takes 5 minutes...)'
.separator " "
.import WEB_mag.dat.1 web_mag

commit;
.print 'data imported'


.print 'creating index...'
begin transaction;
create index idx_mag_num on web_mag (num);
commit;
.print 'index created'


.print 'Number of records imported:'
select count(*) from web_mag;


