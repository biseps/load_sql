--  E Farrell 2014


.print
.print 'input File: WEBwide_mag.dat.1'
.print 'creating table ...'
begin transaction;

create table webwide_mag 
(
    num     real, 
    mag1    real, 
    mag2    real, 
    magBin  real
);

.print 'importing data...'
.separator " "
.import WEBwide_mag.dat.1 webwide_mag

commit;
.print 'data imported'


.print 'creating index...'
begin transaction;
create index idx_magwide_num on webwide_mag (num);
commit;
.print 'index created'


.print 'Number of records imported:'
select count(*) from webwide_mag;


