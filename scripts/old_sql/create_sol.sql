--  E Farrell 2014

.print
.print 'importing ASCII text files'
.print 'into sqlite3 database...'
.print

--  single stars
.read table_webwide_mag.sql
.read table_webwide_pop.sql
.read table_webwide_kepler.sql

--  binaries
--  .read table_web_mag.sql
--  .read table_web_pop.sql
--  .read table_web_kepler.sql


.print
.print 'Tables in database:'
.tables


