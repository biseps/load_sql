--  E Farrell 2014


.print
.print 'input File: WEB_pop.dat.1'
.print 'creating table ...'
begin transaction;

create table web_pop 
(
    num     real, 
    mt1     real, 
    mt2     real, 
    Porb    real,
    birth   real,
    death   real,
    evol    text
);

.print 'importing data...'
.print '(takes 5 minutes...)'
.separator " "
.import WEB_pop.dat.1 web_pop

commit;
.print 'data imported'


.print 'creating index...'
begin transaction;
create index idx_pop_num on web_pop (num);
commit;
.print 'index created'


.print 'Number of records imported:'
select count(*) from web_pop;


