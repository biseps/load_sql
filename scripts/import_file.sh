#!/bin/bash --

# Import a (csv) text file into SQLite:


# main cause of import failures
# is a wrong separator

csv_separator=" "
# csv_separator=","


# Validate arguments
if [ "$#" -ne 3 ];
then
    echo "Usage:"
    echo "\$1 : database name"
    echo "\$2 : input text file (csv type)"
    echo "\$3 : database table name"
    exit 1
fi



# create shell variables
db_name="$1"
file_name="$2"
table_name="$3"



# does database exist?
if [ ! -f "$db_name" ]; then
    echo "fatal: database does not exist:"
    echo "$db_name"
    exit 1
fi


# import file,
# pass SQL script as 'heredoc'

sqlite3 -batch ${db_name} <<EOF
.print
.print 'importing data...'
.print 'may take several minutes...'

.echo on
begin transaction;
.separator "${csv_separator}"
.import "${file_name}" ${table_name}
commit;

select count(*) from ${table_name};
EOF

