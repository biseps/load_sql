#!/bin/bash --


# Create an SQLite table:


# Validate command-line arguments
if [ "$#" -ne 3 ];
then
    echo "Usage:"
    echo "\$1 : database name"
    echo "\$2 : table name"
    echo "\$3 : column definition file e.g. mag.cols, kep.cols"
    exit 1
fi


# read command-line arguments
db_name="$1"
table="$2"
columns_file="$3"


# does the column definitions exist?
if [ ! -f "$columns_file" ]; then
    echo "fatal: column definition file does not exist:"
    echo "$columns_file"
    exit 1
fi


# read contents of definition 
# file into a variable
columns=$(< "$columns_file")



# create sqlite table,
# passing SQL statements as a 'heredoc'

sqlite3 -batch ${db_name} <<EOF
CREATE TABLE ${table} ${columns};
.print
.schema ${table}
EOF

