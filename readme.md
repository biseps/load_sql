
# create SQLite database from text data

Bash scripts which imports data from text files (csv) into a Sqlite database.


# Purpose

Bash script - import text files into SQLite.

Performance: 15 mins for ~ 30 million rows

(switches off transactions during import)



# Usage 


script format

    process_file.sh  {text file}  {table name}  {column definition file}  {sqlite database}   

    {column definition file} - script assumes this is in ./column_defs
    {sqlite database}        - the database file will be created

example

    process_file.sh  planets.csv  planet        planet.cols               myplanets.sqlite



# Requirements

### 1. SQLite 
[SQLite downloads](http://www.sqlite.org/download.html)


### 2. sqlite3 utility
[SQLite3 command line shell](http://www.sqlite.org/sqlite.html)


### 3. Sqlite GUI (Optional)
[SQLite Studio](http://sqlitestudio.pl/) 
![screenshot](http://sqlitestudio.pl/ss/full_36.png)


