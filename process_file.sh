#!/bin/bash --


# Import a text file into SQLite


# Validate arguments
if [ "$#" -lt 4 ];
then
    echo "Usage:"
    echo "\$1 : input text file"
    echo "\$2 : table name"
    echo "\$3 : column definition file e.g. mag.cols, kep.cols"
    echo "\$4 : database name"
    echo "\$5 : (optional) column name to create index on"
    exit 1
fi


# Assign arguments:
FILE="$1"
TABLE="$2"
COLUMN_DEF_FILE="$3"
DB_NAME="$4"


# echo $FILE
# echo $TABLE
# echo $COLUMN_DEF_FILE
# echo $DB_NAME


# index column is optional,
# only created if 5th argument not null
[ -n "$5" ] && IDX_COLUMN="$5"


# Locate various files needed:

# Absolute path to this script, 
THIS_SCRIPT=$(readlink -f "$0")

# Absolute directory this script is in, 
BASE_DIR=$(dirname "$THIS_SCRIPT")

# Assume the scripts are 
# in a sub-directory of $BASE_DIR
SCRIPTS_DIR="$BASE_DIR/scripts"

# Assume column definitions are 
# in a sub-directory of $BASE_DIR
DEFS_FOLDER="$BASE_DIR/column_defs"



# Check file actually exists
if [ ! -f "$FILE" ];
then
    echo fatal: file not found
    echo "$FILE" 
    exit 1
fi



# output useful info
echo 
echo "database     : $DB_NAME"
echo "file         : $FILE"
echo "table        : $TABLE"
echo "definition   : $COLUMN_DEF_FILE"


# import file into Sqlite
"$SCRIPTS_DIR"/create_table.sh  "$DB_NAME"  "$TABLE"  "$DEFS_FOLDER/$COLUMN_DEF_FILE"
"$SCRIPTS_DIR"/import_file.sh   "$DB_NAME"  "$FILE"   "$TABLE"


# create index if neccessary
if [ -n "$IDX_COLUMN" ]
then
     "$SCRIPTS_DIR"/create_index.sh  "$DB_NAME"  "$TABLE"  "$IDX_COLUMN"
fi


